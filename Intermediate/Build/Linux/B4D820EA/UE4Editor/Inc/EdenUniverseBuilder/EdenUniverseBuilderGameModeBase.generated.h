// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef EDENUNIVERSEBUILDER_EdenUniverseBuilderGameModeBase_generated_h
#error "EdenUniverseBuilderGameModeBase.generated.h already included, missing '#pragma once' in EdenUniverseBuilderGameModeBase.h"
#endif
#define EDENUNIVERSEBUILDER_EdenUniverseBuilderGameModeBase_generated_h

#define Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_RPC_WRAPPERS
#define Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEdenUniverseBuilderGameModeBase(); \
	friend EDENUNIVERSEBUILDER_API class UClass* Z_Construct_UClass_AEdenUniverseBuilderGameModeBase(); \
public: \
	DECLARE_CLASS(AEdenUniverseBuilderGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/EdenUniverseBuilder"), NO_API) \
	DECLARE_SERIALIZER(AEdenUniverseBuilderGameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAEdenUniverseBuilderGameModeBase(); \
	friend EDENUNIVERSEBUILDER_API class UClass* Z_Construct_UClass_AEdenUniverseBuilderGameModeBase(); \
public: \
	DECLARE_CLASS(AEdenUniverseBuilderGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/EdenUniverseBuilder"), NO_API) \
	DECLARE_SERIALIZER(AEdenUniverseBuilderGameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEdenUniverseBuilderGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEdenUniverseBuilderGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEdenUniverseBuilderGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEdenUniverseBuilderGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEdenUniverseBuilderGameModeBase(AEdenUniverseBuilderGameModeBase&&); \
	NO_API AEdenUniverseBuilderGameModeBase(const AEdenUniverseBuilderGameModeBase&); \
public:


#define Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEdenUniverseBuilderGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEdenUniverseBuilderGameModeBase(AEdenUniverseBuilderGameModeBase&&); \
	NO_API AEdenUniverseBuilderGameModeBase(const AEdenUniverseBuilderGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEdenUniverseBuilderGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEdenUniverseBuilderGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEdenUniverseBuilderGameModeBase)


#define Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_12_PROLOG
#define Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_RPC_WRAPPERS \
	Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_INCLASS \
	Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Source_EdenUniverseBuilder_EdenUniverseBuilderGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
