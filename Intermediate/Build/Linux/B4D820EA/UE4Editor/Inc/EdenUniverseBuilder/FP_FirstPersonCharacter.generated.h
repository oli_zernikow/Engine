// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef EDENUNIVERSEBUILDER_FP_FirstPersonCharacter_generated_h
#error "FP_FirstPersonCharacter.generated.h already included, missing '#pragma once' in FP_FirstPersonCharacter.h"
#endif
#define EDENUNIVERSEBUILDER_FP_FirstPersonCharacter_generated_h

#define Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_RPC_WRAPPERS
#define Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFP_FirstPersonCharacter(); \
	friend EDENUNIVERSEBUILDER_API class UClass* Z_Construct_UClass_AFP_FirstPersonCharacter(); \
public: \
	DECLARE_CLASS(AFP_FirstPersonCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/EdenUniverseBuilder"), NO_API) \
	DECLARE_SERIALIZER(AFP_FirstPersonCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAFP_FirstPersonCharacter(); \
	friend EDENUNIVERSEBUILDER_API class UClass* Z_Construct_UClass_AFP_FirstPersonCharacter(); \
public: \
	DECLARE_CLASS(AFP_FirstPersonCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/EdenUniverseBuilder"), NO_API) \
	DECLARE_SERIALIZER(AFP_FirstPersonCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFP_FirstPersonCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFP_FirstPersonCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFP_FirstPersonCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFP_FirstPersonCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFP_FirstPersonCharacter(AFP_FirstPersonCharacter&&); \
	NO_API AFP_FirstPersonCharacter(const AFP_FirstPersonCharacter&); \
public:


#define Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFP_FirstPersonCharacter(AFP_FirstPersonCharacter&&); \
	NO_API AFP_FirstPersonCharacter(const AFP_FirstPersonCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFP_FirstPersonCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFP_FirstPersonCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFP_FirstPersonCharacter)


#define Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AFP_FirstPersonCharacter, FirstPersonCameraComponent); }


#define Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_15_PROLOG
#define Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_RPC_WRAPPERS \
	Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_INCLASS \
	Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Source_EdenUniverseBuilder_FP_FirstPerson_FP_FirstPersonCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
